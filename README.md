This little simulator simulates a quiz scenario like those often found in quiz shows like "Wer wird Millionär".
It was originally an assignment for the module "Grafische Benutzeroberflächen" in winter semester 2015 - 2016.
The main goal was to develop a medium sized project which follows the model-view-presenter design pattern using several presenters and views working on only one model.
There is already a set of questions built-in which can be modified to your liking.

QuizSim has the following features:

* Simulation of quiz scenarios
* Overview for all questions plus counter for how often an answer was given and how often the answer was correct
* Resuming a quiz scenario (for example after switching to the overview) and/or starting the simulation anew
* Editor for editing, adding and deleting questions
* Supports questions with arbitrary amount of possible answers

To setup this project just clone/fork it and import in eclipse.

You can get an executable jar file in the [download](https://bitbucket.org/geht/gbo-quiz/downloads) section