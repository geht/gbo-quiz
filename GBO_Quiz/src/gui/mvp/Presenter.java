package gui.mvp;

/**
 * This interface is used for the Model View Presenter design pattern. It
 * provides a presenter which is used in combination with a {@link Model} and a
 * {@link View}.
 * 
 * @author Marwin Rieger
 *
 * @param <M>
 *            the class of the Model which is to be used for this Presenter
 * @param <V>
 *            the class of the View which is to be used for this Presenter
 * @param <P>
 *            the class of the Presenter itself
 */
public interface Presenter<M extends Model, V extends View<V, P>, P extends Presenter<M, V, P>> {

	/**
	 * Sets the Model. Connects the given Model to this Presenter.
	 * 
	 * @param model
	 *            to set
	 */
	public void setModel(M model);

	/**
	 * Sets the View. Connects the given View to this Presenter.
	 * 
	 * @param view
	 *            to set
	 */
	public void setView(V view);

	/**
	 * Gets the View. You should make sure you have set the view before calling
	 * this method.
	 * 
	 * @return the View connected to this Presenter
	 */
	public V getView();
}
