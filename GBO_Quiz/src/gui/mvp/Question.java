package gui.mvp;

import javafx.beans.property.Property;

/**
 * This interface defines how a Question in this Quiz should look like. A
 * Question consists of the Question itself, a set of answers and a (single)
 * correct answer. In addition there is the count of how often the Question has
 * been answered during all game sessions and how often the given answer was
 * correct.
 * 
 * @author Marwin Rieger
 *
 */
public interface Question {

	/**
	 * This Property holds the String of the Question.
	 * 
	 * @return String Property of the Question
	 */
	public Property<String> questionProperty();

	/**
	 * Getter for the correct answer to the Question.
	 * 
	 * @return String of the correct answer
	 */
	public String getCorrectAnswer();

	/**
	 * Getter for the index of the correct answer to the Question.
	 * 
	 * @return index of the correct answer
	 */
	public int getIndexOfCorrectAnswer();

	/**
	 * Getter for all possible answers to this Question. Note that only one of
	 * these is the correct answer.
	 * 
	 * @return array of all possible answers
	 */
	public String[] getAnswers();

	/**
	 * This Property holds the number of how often the Question has been
	 * answered.
	 * 
	 * @return Number Property of the number of given answers
	 */
	public Property<Number> totalAnsweredTimesProperty();

	/**
	 * This Property holds the number of how often the Question has been
	 * correctly answered.
	 * 
	 * @return Number Property of the number of correctly given answers
	 */
	public Property<Number> correctlyAnsweredTimeProperty();

	/**
	 * Resets the number of given answers.
	 */
	public void resetAnsweredTimes();

	/**
	 * Increases the number of given answers by one.
	 */
	public void increaseTotalAnsweredTimes();

	/**
	 * Increases the number of correctly given answers by one.
	 */
	public void increaseCorrectlyAnsweredTimes();
}
