package gui.mvp;

/**
 * This interface is used for the Model View Presenter design pattern. It
 * provides a View which is used in combination with a {@link Presenter}.
 * 
 * @author Marwin Rieger
 *
 * @param <V>
 *            the class of the View itself
 * @param <P>
 *            the class of the Presenter which is to be used for this View
 */
public interface View<V extends View<V, P>, P extends Presenter<?, V, P>> {

	/**
	 * Sets the Presenter. Connects the given Presenter to this View.
	 * 
	 * @param presenter
	 *            to set
	 */
	public void setPresenter(P presenter);
}
