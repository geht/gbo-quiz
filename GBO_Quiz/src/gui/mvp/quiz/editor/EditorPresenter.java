package gui.mvp.quiz.editor;

import gui.mvp.Presenter;
import gui.mvp.Question;
import gui.mvp.quiz.model.QuizModel;
import javafx.scene.control.ButtonType;

/**
 * This Presenter is part of the Model View Presenter design pattern. It
 * presents an editor for Questions and delegates data between the
 * {@link QuizModel} and the {@link EditorView}.
 * 
 * @author Marwin Rieger
 * @see Presenter
 */
public class EditorPresenter implements Presenter<QuizModel, EditorView, EditorPresenter> {
	private QuizModel model;

	private EditorView view;

	/**
	 * Instantiates a new EditorPresenter and connects to the {@link QuizModel}.
	 */
	public EditorPresenter() {
		this.model = QuizModel.getInstance();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gui.mvp.Presenter#setModel(gui.mvp.Model)
	 */
	@Override
	public void setModel(QuizModel model) {
		this.model = model;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gui.mvp.Presenter#setView(gui.mvp.View)
	 */
	@Override
	public void setView(EditorView view) {
		this.view = view;
		view.setList(model.getQuestions());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gui.mvp.Presenter#getView()
	 */
	@Override
	public EditorView getView() {
		return view;
	}

	/**
	 * Shows a dialog to add a new {@link Question}.
	 */
	public void showQuestionAdder() {
		if (view == null) {
			throw new IllegalStateException();
		}

		view.showQuestionEditor(null);
	}

	/**
	 * Shows an Editor for the currently selected {@link Question}.
	 */
	public void showQuestionEditor() {
		if (view == null) {
			throw new IllegalStateException();
		}

		Question question = view.getSelectedQuestion();
		if (question == null) {
			view.showInfoDialog();
		} else {
			view.showQuestionEditor(question);
		}
	}

	/**
	 * Displays a confirmation dialog when deleting a {@link Question}.
	 */
	public void showDeleteQuestion() {
		if (view == null) {
			throw new IllegalStateException();
		}

		Question question = view.getSelectedQuestion();
		if (question == null) {
			view.showInfoDialog();
		} else {
			view.showConfirmationDialog().filter(response -> response == ButtonType.OK)
					.ifPresent(response -> model.deleteQuestion(question));
		}
	}

	/**
	 * Saves the changes made to a {@link Question}.
	 * 
	 * @param oldQuestion
	 *            old Question
	 * @param questionText
	 *            text of the Question
	 * @param index
	 *            of the correct answer
	 * @param answers
	 *            to the Question
	 */
	public void save(Question oldQuestion, String questionText, int index, String[] answers) {
		if (oldQuestion == null) {
			model.addQuestion(questionText, index, answers);
		} else {
			model.replaceQuestion(oldQuestion, questionText, index, answers);
		}
	}
}
