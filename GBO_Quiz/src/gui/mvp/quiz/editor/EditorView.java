package gui.mvp.quiz.editor;

import java.io.IOException;
import java.util.Optional;

import gui.mvp.Question;
import gui.mvp.View;
import gui.mvp.quiz.editor.dialog.EditDialog;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * This View is part of the Model View Presenter design pattern. It provides a
 * view for editing Questions. It uses an FXML file for the description of non
 * dynamic UI elements.
 * 
 * @author Marwin Rieger
 * @see View
 * @see VBox
 *
 */
public class EditorView extends VBox implements View<EditorView, EditorPresenter> {

	/**
	 * The path to the FXML file for this view.
	 */
	public static final String FXML_FILE = "EditorView.fxml";

	/**
	 * The title for the information dialog.
	 */
	public static final String INFO_TITLE = "Information";

	/**
	 * The text for the information dialog.
	 */
	public static final String INFO_DIALOG = "Es muss eine Frage ausgewählt werden!";

	/**
	 * The title for the confirmation dialog.
	 */
	public static final String CONFIRMATION_TITLE = "Achtung";

	/**
	 * The text for the header of the confirmation dialog.
	 */
	public static final String CONFIRMATION_HEADER = "Sie sind dabei die ausgewählte Frage zu löschen.";

	/**
	 * The text for the confirmation dialog.
	 */
	public static final String CONFIRMATION_DIALOG = "Möchten Sie das wirklich?";

	/**
	 * The title for the editor window.
	 */
	public static final String QUESTIONEDITOR_TITLE = "Frage editieren";

	@FXML
	private ListView<Question> listView;

	private EditorPresenter presenter;

	/**
	 * Instantiates a new EditorView. Loads non dynamic UI components from an
	 * FXML file.
	 */
	public EditorView() {
		FXMLLoader loader = new FXMLLoader(getClass().getResource(FXML_FILE));
		loader.setRoot(this);
		loader.setController(this);

		try {
			loader.load();
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	@FXML
	private void addQuestion() {
		presenter.showQuestionAdder();
	}

	@FXML
	private void editQuestion() {
		if (presenter == null) {
			throw new IllegalStateException();
		}

		presenter.showQuestionEditor();
	}

	@FXML
	private void deleteQuestion() {
		if (presenter == null) {
			throw new IllegalStateException();
		}

		presenter.showDeleteQuestion();
	}

	@FXML
	private void onListViewClicked(MouseEvent event) {
		if (event.getButton().equals(MouseButton.PRIMARY) && event.getClickCount() == 2) {
			editQuestion();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gui.mvp.View#setPresenter(gui.mvp.Presenter)
	 */
	@Override
	public void setPresenter(EditorPresenter presenter) {
		this.presenter = presenter;
	}

	/**
	 * Displays a list of Questions.
	 * 
	 * @param questions
	 *            list of questions
	 */
	public void setList(ObservableList<Question> questions) {
		listView.setItems(questions);
	}

	/**
	 * Getter for the currently selected {@link Question}.
	 * 
	 * @return selected question
	 */
	public Question getSelectedQuestion() {
		return listView.getSelectionModel().getSelectedItem();
	}

	/**
	 * Displays the information dialog. This method is to be used when no
	 * {@link Question} has been selected for editing.
	 */
	public void showInfoDialog() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle(INFO_TITLE);
		alert.setHeaderText(null);
		alert.setContentText(INFO_DIALOG);
		alert.showAndWait();
	}

	/**
	 * Displays an confirmation dialog. This method is to be used when a
	 * {@link Question} shall be deleted.
	 * 
	 * 
	 * @return An {@link Optional} that contains the {@link #resultProperty()
	 *         result}. Refer to the {@link Dialog} class documentation for more
	 *         detail.
	 * @see Dialog
	 */
	public Optional<ButtonType> showConfirmationDialog() {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle(CONFIRMATION_TITLE);
		alert.setHeaderText(CONFIRMATION_HEADER);
		alert.setContentText(CONFIRMATION_DIALOG);

		return alert.showAndWait();
	}

	/**
	 * Displays the qiven {@link Question} for editing.
	 * 
	 * @param question
	 */
	public void showQuestionEditor(Question question) {
		EditDialog dialog = new EditDialog(question);
		dialog.setPresenter(presenter);
		Stage stage = new Stage();
		stage.setTitle(QUESTIONEDITOR_TITLE);
		stage.setScene(new Scene(dialog));
		stage.initOwner(this.getScene().getWindow());
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.showAndWait();
		listView.refresh();
	}
}
