package gui.mvp.quiz.editor.dialog;

import java.io.IOException;
import java.util.ArrayList;

import gui.mvp.Question;
import gui.mvp.View;
import gui.mvp.quiz.editor.EditorPresenter;
import gui.mvp.quiz.editor.EditorView;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * This class provides a dialog for editing Quesions.
 * 
 * @author Marwin Rieger
 * @see View
 * @see VBox
 */
public class EditDialog extends VBox implements View<EditorView, EditorPresenter> {

	/**
	 * The path to the FXML file for this view.
	 * 
	 */
	public static final String FXML_FILE = "EditDialog.fxml";

	/**
	 * The spacing used for the Vbox.
	 */
	public static final double ROW_SPACING = 5.0;

	/**
	 * The text for the delete button.
	 */
	public static final String DELETE_BUTTON_TEXT = "Löschen";

	@FXML
	private TextField textField;

	@FXML
	private VBox content;

	private ToggleGroup group;

	private Question question;

	private EditorPresenter presenter;

	private ArrayList<TextField> answerFields;

	/**
	 * Instantiates a new EditDialog. Loads non dynamic UI components from an
	 * FXML file.
	 * 
	 * @param oldQuestion
	 *            question to be edited
	 */
	public EditDialog(Question oldQuestion) {
		this.question = oldQuestion;
		FXMLLoader loader = new FXMLLoader(getClass().getResource(FXML_FILE));
		loader.setRoot(this);
		loader.setController(this);

		try {
			loader.load();
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

		group = new ToggleGroup();
		answerFields = new ArrayList<>();

		if (question != null) {
			injectQuestion();
		}
	}

	private void injectQuestion() {
		textField.setText(question.questionProperty().getValue());

		for (int i = 0; i < question.getAnswers().length; i++) {
			addAnswer(question.getAnswers()[i]);
		}
		group.getToggles().get(question.getIndexOfCorrectAnswer()).setSelected(true);
	}

	private void addAnswer(String text) {
		HBox row = new HBox(ROW_SPACING);
		row.setAlignment(Pos.CENTER_LEFT);

		RadioButton radioButton = new RadioButton();
		radioButton.setToggleGroup(group);
		TextField answerField = new TextField(text);
		answerFields.add(answerField);
		Button button = new Button(DELETE_BUTTON_TEXT);
		button.setOnAction(event -> {
			group.getToggles().remove(radioButton);
			content.getChildren().remove(row);
			answerFields.remove(answerField);
		});

		row.getChildren().addAll(radioButton, answerField, button);
		content.getChildren().add(row);
	}

	@FXML
	private void addAnswer() {
		addAnswer("");
	}

	@FXML
	private void save() {
		String questionText = textField.getText();
		int index = group.getToggles().indexOf(group.getSelectedToggle());
		String[] answers = answerFields.stream().map(answerField -> answerField.getText())
				.filter(string -> !string.isEmpty()).toArray(String[]::new);

		presenter.save(question, questionText, index, answers);
		close();
	}

	@FXML
	private void close() {
		Stage stage = (Stage) getScene().getWindow();
		stage.close();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gui.mvp.View#setPresenter(gui.mvp.Presenter)
	 */
	@Override
	public void setPresenter(EditorPresenter presenter) {
		this.presenter = presenter;
	}
}
