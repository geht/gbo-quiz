package gui.mvp.quiz.overview;

import java.io.IOException;

import gui.mvp.Question;
import gui.mvp.View;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.VBox;

/**
 * This View is part of the Model View Presenter design pattern. It provides an
 * overview for all Questions. It uses an FXML file for the description of non
 * dynamic UI elements.
 * 
 * @author Marwin Rieger
 * @see View
 * @see VBox
 *
 */
public class OverviewView extends VBox implements View<OverviewView, OverviewPresenter> {

	/**
	 * The path to the FXML file for this view.
	 */
	public static final String FXML_FILE = "OverviewView.fxml";

	@FXML
	private TableView<Question> questionTableView;

	@FXML
	private TableColumn<Question, String> questionColumn;

	@FXML
	private TableColumn<Question, Number> answeredColumn;

	@FXML
	private TableColumn<Question, Number> correclyAnsweredColumn;

	private OverviewPresenter presenter;

	/**
	 * Instantiates a new OverviewView. Loads non dynamic UI components from an
	 * FXML file.
	 */
	public OverviewView() {
		FXMLLoader loader = new FXMLLoader(getClass().getResource(FXML_FILE));
		loader.setRoot(this);
		loader.setController(this);

		try {
			loader.load();
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

		questionColumn.setCellValueFactory(item -> item.getValue().questionProperty());
		answeredColumn.setCellValueFactory(item -> item.getValue().totalAnsweredTimesProperty());
		correclyAnsweredColumn.setCellValueFactory(item -> item.getValue().correctlyAnsweredTimeProperty());
	}

	@FXML
	private void resetAnswers() {
		if (presenter == null) {
			throw new IllegalStateException();
		}

		presenter.resetAnswers();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gui.mvp.View#setPresenter(gui.mvp.Presenter)
	 */
	@Override
	public void setPresenter(OverviewPresenter presenter) {
		this.presenter = presenter;
	}

	/**
	 * Displays the given list of {@link Question}s.
	 * 
	 * @param questions
	 *            to display
	 */
	public void setList(ObservableList<Question> questions) {
		questionTableView.setItems(questions);
	}
}
