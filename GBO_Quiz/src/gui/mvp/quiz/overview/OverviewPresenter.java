package gui.mvp.quiz.overview;

import gui.mvp.Presenter;
import gui.mvp.Question;
import gui.mvp.quiz.model.QuizModel;

/**
 * This Presenter is part of the Model View Presenter design pattern. It
 * presents an overview of all Questions and delegates data between the
 * {@link QuizModel} and the {@link OverviewView}.
 * 
 * @author Marwin Rieger
 * @see Presenter
 */
public class OverviewPresenter implements Presenter<QuizModel, OverviewView, OverviewPresenter> {
	private QuizModel model;

	private OverviewView view;

	/**
	 * Instantiates a new OverviewPresenter and connects to the
	 * {@link QuizModel}.
	 */
	public OverviewPresenter() {
		this.model = QuizModel.getInstance();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gui.mvp.Presenter#setModel(gui.mvp.Model)
	 */
	@Override
	public void setModel(QuizModel model) {
		this.model = model;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gui.mvp.Presenter#setView(gui.mvp.View)
	 */
	@Override
	public void setView(OverviewView view) {
		this.view = view;
		this.view.setList(model.getQuestions());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gui.mvp.Presenter#getView()
	 */
	@Override
	public OverviewView getView() {
		return view;
	}

	/**
	 * Calls the {@link QuizModel} to reset the number of given answers for
	 * every {@link Question}.
	 */
	public void resetAnswers() {
		model.resetAnswerCounters();
	}
}
