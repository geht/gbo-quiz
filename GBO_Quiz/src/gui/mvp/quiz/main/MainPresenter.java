package gui.mvp.quiz.main;

import gui.mvp.Presenter;
import gui.mvp.quiz.editor.EditorPresenter;
import gui.mvp.quiz.editor.EditorView;
import gui.mvp.quiz.model.QuizModel;
import gui.mvp.quiz.overview.OverviewPresenter;
import gui.mvp.quiz.overview.OverviewView;
import gui.mvp.quiz.quiz.QuizPresenter;
import gui.mvp.quiz.quiz.QuizView;
import javafx.application.Platform;
import javafx.geometry.Point3D;

/**
 * This Presenter is part of the Model View Presenter design pattern. It
 * presents the main window and delegates between the different presenters. It
 * uses a {@link QuizPresenter}, an {@link OverviewPresenter} and an
 * {@link EditorPresenter}.
 * 
 * @author Marwin Rieger
 * @see Presenter
 *
 */
public class MainPresenter implements Presenter<QuizModel, MainView, MainPresenter> {
	private MainView view;

	private QuizPresenter quizPresenter;

	private OverviewPresenter overviewPresenter;

	private EditorPresenter editorPresenter;

	/**
	 * Setter for the {@link QuizPresenter}.
	 * 
	 * @param quizPresenter
	 *            to set
	 */
	public void setQuizPresenter(QuizPresenter quizPresenter) {
		this.quizPresenter = quizPresenter;
	}

	/**
	 * Setter for the {@link OverviewPresenter}.
	 * 
	 * @param overviewPresenter
	 *            to set
	 */
	public void setOverviewPresenter(OverviewPresenter overviewPresenter) {
		this.overviewPresenter = overviewPresenter;
	}

	/**
	 * Setter for the {@link EditorPresenter}.
	 * 
	 * @param editorPresenter
	 *            to set
	 */
	public void setEditorPresenter(EditorPresenter editorPresenter) {
		this.editorPresenter = editorPresenter;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gui.mvp.Presenter#setView(gui.mvp.View)
	 */
	@Override
	public void setView(MainView view) {
		this.view = view;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gui.mvp.Presenter#getView()
	 */
	@Override
	public MainView getView() {
		return view;
	}

	/**
	 * Uses the {@link QuizPresenter} to show the {@link QuizView}.
	 */
	public void showQuizView() {
		if (view == null) {
			throw new IllegalStateException();
		}

		view.setMainContent(quizPresenter.getView());
	}

	/**
	 * Resets the Quiz and uses the {@link QuizPresenter} to show a new
	 * {@link QuizView}.
	 */
	public void showNewQuizView() {
		if (quizPresenter == null || view == null) {
			throw new IllegalStateException();
		}

		quizPresenter.resetQuiz();
		quizPresenter.showCurrentQuestion();
		view.setMainContent(quizPresenter.getView());
		view.enableResumeButton();
	}

	/**
	 * Uses the {@link OverviewPresenter} to show the {@link OverviewView}.
	 */
	public void showOverviewView() {
		if (overviewPresenter == null || view == null) {
			throw new IllegalStateException();
		}

		view.setMainContent(overviewPresenter.getView());
	}

	/**
	 * Uses the {@link EditorPresenter} to show the {@link EditorView} and
	 * disables resuming the Quiz.
	 */
	public void showEditorView() {
		if (editorPresenter == null || view == null) {
			throw new IllegalStateException();
		}

		view.setMainContent(editorPresenter.getView());
		view.disableResumeButton();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gui.mvp.Presenter#setModel(gui.mvp.Model)
	 */
	@Override
	public void setModel(QuizModel model) {
		// this one doesn't need a model
	}

	/**
	 * Silly little method... don't even mind it.
	 * 
	 * @param count
	 *            number of rotations
	 */
	public void rotate(int count) {
		view.setRotationAxis(new Point3D(1, 0, 0));
		new Thread(() -> {
			int times = count;

			while (times > 0) {
				for (int i = 0; i < 360; i++) {
					try {
						Thread.sleep(1);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					final int j = i;
					Platform.runLater(() -> {
						view.setRotate(j);
					});
				}
				times--;
			}
		}).start();
	}
}
