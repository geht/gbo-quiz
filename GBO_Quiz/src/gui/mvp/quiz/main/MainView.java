package gui.mvp.quiz.main;

import java.io.IOException;

import gui.mvp.View;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * This View is part of the Model View Presenter design pattern. It provides the
 * main view. It uses an FXML file for the description of non dynamic UI
 * elements.
 * 
 * @author Marwin Rieger
 * @see View
 * @see BorderPane
 * 
 * @author Marwin Rieger
 *
 */
public class MainView extends BorderPane implements View<MainView, MainPresenter> {

	/**
	 * The path to the FXML file for this view.
	 */
	public static final String FXML_FILE = "MainView.fxml";

	private static final String INFO_TITLE = "Über QuizSim";

	private static final String INFO_DIALOG = "QuizSim version 0.2\nby Marwin Rieger";

	@FXML
	private Button resumeQuizButton;

	@FXML
	private MenuItem resumeQuizMenu;

	private MainPresenter presenter;

	/**
	 * Instantiates a new MainView. Loads non dynamic UI components from an FXML
	 * file.
	 */
	public MainView() {
		FXMLLoader loader = new FXMLLoader(getClass().getResource(FXML_FILE));
		loader.setRoot(this);
		loader.setController(this);

		try {
			loader.load();
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	@FXML
	private void startQuiz() {
		if (presenter == null) {
			throw new IllegalStateException();
		}

		presenter.showNewQuizView();
	}

	@FXML
	private void resumeQuiz() {
		if (presenter == null) {
			throw new IllegalStateException();
		}

		presenter.showQuizView();
	}

	@FXML
	private void showOverview() {
		if (presenter == null) {
			throw new IllegalStateException();
		}

		presenter.showOverviewView();
	}

	@FXML
	private void showEdior() {
		if (presenter == null) {
			throw new IllegalStateException();
		}

		presenter.showEditorView();
	}

	@FXML
	private void close() {
		Stage stage = (Stage) this.getScene().getWindow();
		stage.close();
	}

	@FXML
	private void about() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle(INFO_TITLE);
		alert.setHeaderText(null);
		alert.setContentText(INFO_DIALOG);
		presenter.rotate(1);
		alert.showAndWait();
		presenter.rotate(1);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gui.mvp.View#setPresenter(gui.mvp.Presenter)
	 */
	@Override
	public void setPresenter(MainPresenter presenter) {
		this.presenter = presenter;
	}

	/**
	 * Sets the center to the given pane. This method is to be used to change
	 * from one view to another.
	 * 
	 * @param pane
	 *            to set
	 */
	public void setMainContent(Pane pane) {
		this.setCenter(pane);
	}

	/**
	 * Disables resuming the Quiz.
	 */
	public void disableResumeButton() {
		resumeQuizButton.setDisable(true);
		resumeQuizMenu.setDisable(true);
	}

	/**
	 * Enables resuming the Quiz.
	 */
	public void enableResumeButton() {
		resumeQuizButton.setDisable(false);
		resumeQuizMenu.setDisable(false);
	}
}
