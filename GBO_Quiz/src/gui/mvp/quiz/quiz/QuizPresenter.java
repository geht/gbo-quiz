package gui.mvp.quiz.quiz;

import gui.mvp.Presenter;
import gui.mvp.Question;
import gui.mvp.quiz.model.QuizModel;

/**
 * This Presenter is part of the Model View Presenter design pattern. It
 * presents a quiz like view of the Questions and delegates data between the
 * {@link QuizModel} and the {@link QuizView}.
 * 
 * @author Marwin Rieger
 * @see Presenter
 */
public class QuizPresenter implements Presenter<QuizModel, QuizView, QuizPresenter> {
	private QuizModel model;

	private QuizView view;

	/**
	 * Instantiates a new EditorPresenter and connects to the {@link QuizModel}.
	 */
	public QuizPresenter() {
		this.setModel(QuizModel.getInstance());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gui.mvp.Presenter#setModel(gui.mvp.Model)
	 */
	@Override
	public void setModel(QuizModel model) {
		this.model = model;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gui.mvp.Presenter#setView(gui.mvp.View)
	 */
	@Override
	public void setView(QuizView view) {
		this.view = view;
	}

	/**
	 * Resets the Quiz.
	 */
	public void resetQuiz() {
		if (view == null) {
			throw new IllegalStateException();
		}

		model.resetQuiz();
		view.enableButton();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gui.mvp.Presenter#getView()
	 */
	@Override
	public QuizView getView() {
		return view;
	}

	/**
	 * Shows the current {@link Question}.
	 */
	public void showCurrentQuestion() {
		if (view == null) {
			throw new IllegalStateException();
		}

		if (model.currentQuestion() == null) {
			view.showEndCard();
			return;
		}

		view.setHeadLine(model.currentQuestion().questionProperty().getValue());
		view.setAnswers(model.currentQuestion().getAnswers());
	}

	/**
	 * Shows the next {@link Question}.
	 */
	public void showNextQuestion() {
		if (view == null) {
			throw new IllegalStateException();
		}

		String answer = view.getAnswer();

		if (answer != null) {
			model.currentQuestion().increaseTotalAnsweredTimes();

			if (model.currentQuestion().getCorrectAnswer().equals(answer)) {
				model.currentQuestion().increaseCorrectlyAnsweredTimes();
			}
		}

		model.nextQuestion();
		showCurrentQuestion();
	}
}
