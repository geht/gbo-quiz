package gui.mvp.quiz.quiz;

import java.io.IOException;

import gui.mvp.Question;
import gui.mvp.View;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.VBox;

/**
 * This View is part of the Model View Presenter design pattern. It provides a
 * quiz like view of the Questions. It uses an FXML file for the description of
 * non dynamic UI elements.
 * 
 * @author Marwin Rieger
 * @see View
 * @see VBox
 *
 */
public class QuizView extends VBox implements View<QuizView, QuizPresenter> {

	/**
	 * The path to the FXML file for this view.
	 */
	public static final String FXML_FILE = "QuizView.fxml";

	/**
	 * The message to be presented when the Quiz has come to an end.
	 */
	public static final String END = "Ende des Quiz";

	@FXML
	private Label headLine;

	@FXML
	private Button nextButton;

	@FXML
	private VBox content;

	private QuizPresenter presenter;

	private ToggleGroup group;

	/**
	 * Instantiates a new QuizView. Loads non dynamic UI components from an FXML
	 * file.
	 */
	public QuizView() {
		FXMLLoader loader = new FXMLLoader(getClass().getResource(FXML_FILE));
		loader.setRoot(this);
		loader.setController(this);

		try {
			loader.load();
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

		group = new ToggleGroup();
	}

	@FXML
	private void nextQuestion() {
		if (presenter == null) {
			throw new IllegalStateException();
		}

		presenter.showNextQuestion();
	}

	private void clearAnswers() {
		content.getChildren().clear();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gui.mvp.View#setPresenter(gui.mvp.Presenter)
	 */
	@Override
	public void setPresenter(QuizPresenter presenter) {
		this.presenter = presenter;
	}

	/**
	 * Disables the button to get to the next {@link Question} making it
	 * impossible to proceed further.
	 */
	public void disableButton() {
		nextButton.setDisable(true);
	}

	/**
	 * Enables the button to get to the next {@link Question} making it possible
	 * to proceed further.
	 */
	public void enableButton() {
		nextButton.setDisable(false);
	}

	/**
	 * Sets the headline text of this view. This method is used to display
	 * {@link Question}s as well as the end message.
	 * 
	 * @param text
	 *            to be displayed
	 */
	public void setHeadLine(String text) {
		headLine.setText(text);
	}

	/**
	 * Displays the given answers.
	 * 
	 * @param answers
	 *            to be displayed
	 */
	public void setAnswers(String... answers) {
		clearAnswers();
		group = new ToggleGroup();

		for (int i = 0; i < answers.length; i++) {
			RadioButton rb = new RadioButton(answers[i]);
			rb.setToggleGroup(group);
			content.getChildren().add(rb);
		}
	}

	/**
	 * Displays the end card which is shown when the Quiz has come to an end.
	 */
	public void showEndCard() {
		headLine.setText(END);
		clearAnswers();
		disableButton();
	}

	/**
	 * Returns the selected answer.
	 * 
	 * @return selected answer
	 */
	public String getAnswer() {
		RadioButton rb = (RadioButton) group.getSelectedToggle();

		if (rb == null) {
			return null;
		} else {
			return rb.getText();
		}
	}
}