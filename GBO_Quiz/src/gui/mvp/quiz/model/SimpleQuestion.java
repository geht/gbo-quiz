package gui.mvp.quiz.model;

import gui.mvp.Question;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * A simple implementation of {@link Question}. A simple Question consists of a
 * String representation of the Question, a set of possible answers and a
 * correct answer.
 * 
 * @author Marwin Rieger
 * @see Question
 */
public class SimpleQuestion implements Question {
	private SimpleStringProperty question;

	private String[] answers;

	private int indexOfCorrectAnswer;

	private SimpleIntegerProperty totalAnsweredTimes;

	private SimpleIntegerProperty correctlyAnsweredTimes;

	/**
	 * Instatiates a new {@link SimpleQuestion} with the given parameters.
	 * 
	 * @param question
	 *            as string
	 * @param indexOfCorrectAnswer
	 *            index of the correct answer
	 * @param answers
	 *            array of possible answers
	 */
	public SimpleQuestion(String question, int indexOfCorrectAnswer, String... answers) {
		this.question = new SimpleStringProperty(question);
		this.indexOfCorrectAnswer = indexOfCorrectAnswer;
		this.answers = answers;
		this.totalAnsweredTimes = new SimpleIntegerProperty();
		this.correctlyAnsweredTimes = new SimpleIntegerProperty();
		resetAnsweredTimes();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return question.getValue();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gui.mvp.Question#getCorrectAnswer()
	 */
	@Override
	public String getCorrectAnswer() {
		return answers[indexOfCorrectAnswer];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gui.mvp.Question#getIndexOfCorrectAnswer()
	 */
	@Override
	public int getIndexOfCorrectAnswer() {
		return indexOfCorrectAnswer;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gui.mvp.Question#getAnswers()
	 */
	@Override
	public String[] getAnswers() {
		return answers;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gui.mvp.Question#resetAnsweredTimes()
	 */
	@Override
	public void resetAnsweredTimes() {
		totalAnsweredTimes.setValue(0);
		correctlyAnsweredTimes.setValue(0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gui.mvp.Question#increaseTotalAnsweredTimes()
	 */
	@Override
	public void increaseTotalAnsweredTimes() {
		totalAnsweredTimes.setValue(totalAnsweredTimes.getValue() + 1);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gui.mvp.Question#increaseCorrectlyAnsweredTimes()
	 */
	@Override
	public void increaseCorrectlyAnsweredTimes() {
		correctlyAnsweredTimes.setValue(correctlyAnsweredTimes.getValue() + 1);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gui.mvp.Question#questionProperty()
	 */
	@Override
	public StringProperty questionProperty() {
		return question;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gui.mvp.Question#totalAnsweredTimesProperty()
	 */
	@Override
	public IntegerProperty totalAnsweredTimesProperty() {
		return totalAnsweredTimes;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gui.mvp.Question#correctlyAnsweredTimeProperty()
	 */
	@Override
	public IntegerProperty correctlyAnsweredTimeProperty() {
		return correctlyAnsweredTimes;
	}
}
