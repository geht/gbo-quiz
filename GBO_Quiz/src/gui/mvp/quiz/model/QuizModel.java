package gui.mvp.quiz.model;

import java.util.Iterator;

import gui.mvp.Model;
import gui.mvp.Question;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * This View is part of the Model View Presenter design pattern. It holds a set
 * of {@link Question}s and provides methods to modify questions and
 * start/restart the Quiz.
 * 
 * @author Marwin Rieger
 * @see Model
 */
public class QuizModel implements Model {
	private static final QuizModel INSTANCE = new QuizModel();

	private ObservableList<Question> questions;

	private Iterator<Question> iterator;

	private Question currentQuestion;

	/**
	 * Returns the one and only instance of this class.
	 * 
	 * @return instance
	 */
	public static QuizModel getInstance() {
		return INSTANCE;
	}

	private QuizModel() {
		questions = FXCollections.observableArrayList();
		ceateSampleData();
		resetQuiz();
	}

	private void ceateSampleData() {
		questions.add(
				new SimpleQuestion("Wenn das Wetter gut ist, wird der Bauer bestimmt den Eber, das Ferkel und ...?", 2,
						"einen draufmachen", "die Nacht durchzechen", "die Sau rauslassen", "auf die Kacke hauen"));
		questions.add(new SimpleQuestion("Was ist meist ziemlich viel?", 0, "stolze Summe", "selbstbewusste Differenz",
				"arroganter Quotient", "hochmütiges Produkt"));
		questions.add(new SimpleQuestion("Wessen Genesung schnell voranschreitet, der erholt sich ...?", 2,
				"hinguckends", "anschauends", "zusehends", "glotzends"));
		questions.add(new SimpleQuestion("Natürlich spielten musikalische Menschen auch im ...?", 3, "Westsaxo Fon",
				"Nordklari Nette", "Südpo Saune", "Ostblock Flöte"));
		questions.add(new SimpleQuestion("Wobei gibt es keine geregelten Öffnungszeiten?", 3, "Baumärkte",
				"Möbelhäuser", "Teppichgeschäfte", "Fensterläden"));
		questions.add(
				new SimpleQuestion("Was war bereits seit Mai 1969 ein beliebtes Zahlungsmittel im europäischen Raum?",
						1, "Euronoten", "Eurocheques", "Euroscheine", "Euromünzen"));
		questions.add(new SimpleQuestion("Malu Dreyer profitierte Anfang des Jahres von ...?", 3,
				"Oettingers Sattelstange", "Veltins Fahrradkette", "Diebels Vorderreifen", "Becks Rücktritt"));
		questions.add(new SimpleQuestion("Woraus besteht in der Regel eine Entourage?", 2, "Baguette & Rotwein",
				"Mascara & Lidschatten", "Freunde & Bekannte", "Sofa & Sessel"));
		questions.add(new SimpleQuestion("Was haben die Hollywood-Stars Gosling, Reynolds und Phillippe gemeinsam?", 0,
				"Vorname Ryan", "Ex-Frau Megan Fox", "Geburtsjahr 1978", "irische Staatsbürgerschaft"));
		questions.add(new SimpleQuestion(
				"Welche beiden Staaten einigten sich Ende 2012 über die Festsetzung eines Grenzverlaufs?", 2,
				"Deutschland & Australien", "Polen & Südafrika", "Dänemark & Kanada", "Österreich & Japan"));
		questions.add(
				new SimpleQuestion("Seine drei Weltmeister-Titel erfuhr sich Sebastian Vettel mit Motoren von ...?", 2,
						"Ferrari", "Mercedes", "Renault", "Toyota"));
		questions.add(new SimpleQuestion(
				"Welcher General vertrieb im 19. Jahrhundert die Mexikaner aus dem heutigen US-Bundesstaat Texas?", 1,
				"John Denver", "Sam Houston", "Michael Miami", "Phil A. Delphia"));
		questions.add(new SimpleQuestion(
				"Der Text welches dieser berühmten Songs ist ganz offensichtlich an eine Prostituierte gerichtet?", 3,
				"Angie von den Stones", "Manilows Mandy", "Jacksons Billie Jean", "Roxanne von The Police"));
		questions.add(new SimpleQuestion(
				"Was soll in bestimmten Abständen nach der sogenannten ABCDE-Regel kontrolliert werden?", 2,
				"Komposthaufen im Garten", "Luftdruck der Autoreifen", "Leberflecken auf der Haut",
				"Aktienfonds bei der Bank"));
		questions.add(new SimpleQuestion("Wer sollte sich mit der Zwanzig nach vier-Stellung auskennen?", 2,
				"Fahrlehrer", "Karatemeister", "Kellner", "Landschaftsarchitekt"));
	}

	/**
	 * Resets the Quiz.
	 */
	public void resetQuiz() {
		iterator = questions.iterator();
		nextQuestion();
	}

	/**
	 * Returns a list of all the Questions.
	 * 
	 * @return list of questions
	 */
	public ObservableList<Question> getQuestions() {
		return questions;
	}

	/**
	 * Getter for the current {@link Question}.
	 * 
	 * @return current question
	 */
	public Question currentQuestion() {
		return currentQuestion;
	}

	/**
	 * Proceeds the Quiz to the next {@link Question}.
	 */
	public void nextQuestion() {
		if (iterator.hasNext()) {
			currentQuestion = iterator.next();
		} else {
			currentQuestion = null;
		}
	}

	/**
	 * Resets the count of given answer for every {@link Question}.
	 * 
	 * @see Question
	 */
	public void resetAnswerCounters() {
		for (Question q : questions) {
			q.resetAnsweredTimes();
		}
	}

	/**
	 * Removes the given question from the set of {@link Question}s.
	 * 
	 * @param question
	 */
	public void deleteQuestion(Question question) {
		questions.remove(question);
	}

	/**
	 * Adds a new {@link SimpleQuestion} to the set of {@link Question}s.
	 * 
	 * @param questionText
	 *            question as String
	 * @param index
	 *            of correct answer
	 * @param answers
	 *            array of possible answers
	 */
	public void addQuestion(String questionText, int index, String[] answers) {
		if (questionText == null || questionText.isEmpty()) {
			return;
		}

		if (answers.length <= 0) {
			return;
		}

		if (index < 0 || index >= answers.length) {
			return;
		}

		questions.add(new SimpleQuestion(questionText, index, answers));
	}

	/**
	 * Replaces a {@link Question} with a new {@link SimpleQuestion}.
	 * 
	 * @param oldQuestion
	 *            old Question
	 * @param questionText
	 *            question as String
	 * @param index
	 *            of correct answer
	 * @param answers
	 *            array of possible answers
	 */
	public void replaceQuestion(Question oldQuestion, String questionText, int index, String[] answers) {
		if (questionText == null || questionText.isEmpty()) {
			return;
		}

		if (answers.length <= 0) {
			return;
		}

		if (index < 0 || index >= answers.length) {
			return;
		}

		questions.set(questions.indexOf(oldQuestion), new SimpleQuestion(questionText, index, answers));
	}
}
