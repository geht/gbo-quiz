package gui.mvp.quiz;

import gui.mvp.Model;
import gui.mvp.quiz.editor.EditorPresenter;
import gui.mvp.quiz.editor.EditorView;
import gui.mvp.quiz.main.MainPresenter;
import gui.mvp.quiz.main.MainView;
import gui.mvp.quiz.model.QuizModel;
import gui.mvp.quiz.overview.OverviewPresenter;
import gui.mvp.quiz.overview.OverviewView;
import gui.mvp.quiz.quiz.QuizPresenter;
import gui.mvp.quiz.quiz.QuizView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * This is the main entry point for the Application. The Main class initializes
 * all of the Presenters and Views, connects them as necessary and creates a new
 * window to display the Quiz. Using an extended Model View Presenter design
 * pattern there is a {@link QuizModel} and a {@link MainPresenter} which
 * delegates tasks to several sub presenters. There is only one {@link Model}
 * which holds all the data. Each sub presenter delegates data between its own
 * view, the model and the {@link MainPresenter}. Beside the
 * {@link MainPresenter} with its {@link MainView} there is the
 * {@link QuizPresenter} with its {@link QuizView}, the
 * {@link OverviewPresenter} with its {@link OverviewView} and the
 * {@link EditorPresenter} with its {@link EditorView}. Each of these presenters
 * and views fulfill their own task.
 * 
 * @author Marwin Rieger
 * @see Application
 */
public class Main extends Application {
	/**
	 * Title of this Application.
	 */
	public static final String TITLE = "QuizSim";

	/*
	 * (non-Javadoc)
	 * 
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@Override
	public void start(Stage primaryStage) throws Exception {
		MainPresenter mainPresenter = initApplication();
		mainPresenter.showNewQuizView();
		primaryStage.setTitle(TITLE);
		primaryStage.setScene(new Scene(mainPresenter.getView()));
		primaryStage.show();
	}

	/**
	 * This method initializes the Application. It creates all needed Presenters
	 * and Views and connects them as necessary.
	 * 
	 * @return main presenter
	 */
	private MainPresenter initApplication() {
		MainPresenter mainPresenter = new MainPresenter();
		MainView mainView = new MainView();
		QuizPresenter quizPresenter = new QuizPresenter();
		QuizView quizView = new QuizView();
		OverviewPresenter overviewPresenter = new OverviewPresenter();
		OverviewView overviewView = new OverviewView();
		EditorPresenter editorPresenter = new EditorPresenter();
		EditorView editorView = new EditorView();

		mainPresenter.setView(mainView);
		mainPresenter.setQuizPresenter(quizPresenter);
		mainPresenter.setOverviewPresenter(overviewPresenter);
		mainPresenter.setEditorPresenter(editorPresenter);
		mainView.setPresenter(mainPresenter);

		quizPresenter.setView(quizView);
		quizView.setPresenter(quizPresenter);

		overviewPresenter.setView(overviewView);
		overviewView.setPresenter(overviewPresenter);

		editorPresenter.setView(editorView);
		editorView.setPresenter(editorPresenter);

		return mainPresenter;
	}

	/**
	 * Main entry point. Calls the launch method of the Application.
	 * 
	 * @param args
	 *            arguments
	 */
	public static void main(String[] args) {
		launch(args);
	}
}
