package gui.mvp;

/**
 * This interface is a marker interface to identify Classes as some type of
 * Model. It is used for the Model View Presenter design pattern.
 * 
 * @author Marwin Rieger
 *
 */
public interface Model {

}
